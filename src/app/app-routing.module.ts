import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardPaymentFormComponent } from './components/card-payment-form/card-payment-form.component';
import { HomeComponent } from './components/home/home.component';
import { MotorPolicyViewComponent } from './components/motor-policy-view/motor-policy-view.component';
import { MotorPolicyComponent } from './components/motor-policy/motor-policy.component';
import { PaymobCallbackComponent } from './components/paymob-callback/paymob-callback.component';
import { SaveCardComponent } from './components/save-card/save-card.component';

const routes: Routes = [
  {
    path:"paymob/callback", 
    component:PaymobCallbackComponent
  }, 
  {
    path:"savecard", 
    component: SaveCardComponent
  },
  {
    path:"buy", 
    component:MotorPolicyViewComponent
  },
  {
    path:"payment-form", 
    component:CardPaymentFormComponent
  },
  {
    path:"", 
    component:HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
