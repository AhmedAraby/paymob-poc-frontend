/**
 * this component is responsible to 
 * 1- show the axa motor policy to be sold, 
 * 2- and to create order for the axa motor policy 
 * 3- and to create payment request for the order created to this motor policy
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PolicyOrderModel } from 'src/app/models/policy-order.model';
import { PaymobPaymentTokenModel } from '../../models/paymob/paymob-payment-token.model';
import { PaymobPaymentTokenService } from '../../services/paymob-paymentToken.service';
import { PaymobOrderService } from 'src/app/services/paymob-order.service';
import { PaymobCustomerBillingDataModel } from 'src/app/models/paymob/paymob-customer-billing-data.model';
import { PaymentTokenRequestModel } from 'src/app/models/payment-token-request.model';
import { FormControl, Validators } from '@angular/forms';
import { PolicyDataService } from 'src/app/services/policy-data.service';

@Component({
  selector: 'app-motor-policy',
  templateUrl: './motor-policy.component.html',
  styleUrls: ['./motor-policy.component.css']
})

export class MotorPolicyComponent implements OnInit, OnDestroy 
{

  // data to receive
  orderId: string = null;
  paymentToken: string ="";
  
  // data to send
  onlineCardIntegrationId: string = "622544"; // online card test integeration
  customerId = 1;

  // subs 
  paymentTokenSub=null;
  paymobOrderSub = null;

  // patterns
  price_pattern: string = "^[1-9]+[0-9]*$";
  
  // form controls 
  price: FormControl = new FormControl("", [Validators.pattern(this.price_pattern), Validators.required]);

  constructor(
    private paymentService: PaymobPaymentTokenService,
    private paymobOrderService: PaymobOrderService, 
    private policyDataService: PolicyDataService)
  {
  }

  ngOnInit(): void {
  }

  getPaymobPaymentToken()
  {
    if(this.price.valid == false){
      alert("invalid price");
      return ;
    }
    
    const amount_cents = this.price.value * 100 + "";
    console.log("amount cents is : " + amount_cents);
    
    const customerBillingDataModel: PaymobCustomerBillingDataModel = new PaymobCustomerBillingDataModel(
      "Ahmed","Araby",
      "aaraby@sumerge.com", "01068482084");

    const policyOrder: PolicyOrderModel = new PolicyOrderModel(
      "1", amount_cents, 3700,
       "EGP", false);
    
    let paymentTokenRquestModel: PaymentTokenRequestModel = new PaymentTokenRequestModel(
      policyOrder,
      customerBillingDataModel
    );
        
    this.paymentService.getpaymentToken(paymentTokenRquestModel).subscribe(
      (paymentToken: PaymobPaymentTokenModel)=>{
        this.paymentToken = paymentToken.token; 
        this.paymentService.paymobPaymentTokenSubject.next(paymentToken);
      }, 
      (resp)=>{
        console.log("error resp is : " , resp);
        alert("failed to get payment request")
      }
    )
  }

  onPriceChange()
  {
    if(this.price.valid)
      this.policyDataService.priceSubject.next(this.price.value);
  }
 
  ngOnDestroy()
  {
    if(this.paymentTokenSub)
      this.paymentTokenSub.unsubscribe();
    if(this.paymobOrderSub)
      this.paymobOrderSub.unsubscribe();
  }

}
