import { Component, Input, OnInit } from '@angular/core';
import { CardTokenModel } from 'src/app/models/card-token.model';
import { PaymobCustomerBillingDataModel } from 'src/app/models/paymob/paymob-customer-billing-data.model';
import { PolicyOrderModel } from 'src/app/models/policy-order.model';
import { RecurrentPaymentRequestModel } from 'src/app/models/recurrent-payment-request.model';
import { CardTokensService } from 'src/app/services/card-tokens.service';
import { PolicyDataService } from 'src/app/services/policy-data.service';

@Component({
  selector: 'app-card-tokens',
  templateUrl: './card-tokens.component.html',
  styleUrls: ['./card-tokens.component.css']
})
export class CardTokensComponent implements OnInit {

  cardTokens: CardTokenModel[] = [];
  constructor(
    private cardTokenService: CardTokensService, 
    private policyDataService:PolicyDataService) { 
    this.cardTokenService.getTokensByCustomerId(1).subscribe(
      (cardTokens: CardTokenModel[])=>{
        this.cardTokens = cardTokens;
      }, 
      ()=>{
        alert("failed to get tokens");
      }
    )
  }

  ngOnInit(): void {
  }

  payWithSavedCard(cardToken:CardTokenModel)
  {
    let price = this.policyDataService.priceSubject.value;
    if(price == null){
      alert("please enter a valid price");
      return ;
    }

    let amount_cents = price * 100 + "";

    const customerBillingDataModel: PaymobCustomerBillingDataModel = new PaymobCustomerBillingDataModel(
      "Ahmed","Araby",
      "aaraby@sumerge.com", "01068482084");

    const policyOrder: PolicyOrderModel = new PolicyOrderModel(
      "1", amount_cents, 3700,
       "EGP", false);
      
    const recurrentPaymentRequestModel: RecurrentPaymentRequestModel = new RecurrentPaymentRequestModel(
      cardToken.token, 
      policyOrder, 
      customerBillingDataModel
    );


    this.cardTokenService.payWithSavedCard(recurrentPaymentRequestModel).subscribe(
      (resp)=>{
        alert("payment processed");
      }, 
      ()=>{
        alert("failed to pay with saved token");
      }
    );
  }

}
