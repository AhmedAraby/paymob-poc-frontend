import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTokensComponent } from './card-tokens.component';

describe('CardTokensComponent', () => {
  let component: CardTokensComponent;
  let fixture: ComponentFixture<CardTokensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardTokensComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
