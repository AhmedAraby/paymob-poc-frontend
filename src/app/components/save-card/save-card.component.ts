import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { PaymentTokenRequestModel } from 'src/app/models/payment-token-request.model';
import { PaymobCustomerBillingDataModel } from 'src/app/models/paymob/paymob-customer-billing-data.model';
import { PaymobPaymentTokenModel } from 'src/app/models/paymob/paymob-payment-token.model';
import { PolicyOrderModel } from 'src/app/models/policy-order.model';
import { PaymobPaymentTokenService } from 'src/app/services/paymob-paymentToken.service';

@Component({
  selector: 'app-save-card',
  templateUrl: './save-card.component.html',
  styleUrls: ['./save-card.component.css']
})
export class SaveCardComponent implements OnInit {
  paymentToken: string;

  constructor(
    private domSanitizer: DomSanitizer, 
    private paymentService: PaymobPaymentTokenService) { 
      this.getPaymentToken();
    }

  ngOnInit(): void {
  }

  getPaymentToken()
  {
    const customerBillingDataModel: PaymobCustomerBillingDataModel = new PaymobCustomerBillingDataModel(
      "Ahmed","Araby",
      "aaraby@sumerge.com", "01068482084");

    const amount_cents_to_save_card = "100";

    const policyOrder: PolicyOrderModel = new PolicyOrderModel(
      "1", amount_cents_to_save_card, 3700,
       "EGP", false);
    
    let paymentTokenRquestModel: PaymentTokenRequestModel = new PaymentTokenRequestModel(
      policyOrder,
      customerBillingDataModel
    );
        
    this.paymentService.getpaymentToken(paymentTokenRquestModel).subscribe(
      (paymentToken: PaymobPaymentTokenModel)=>{
        this.paymentToken = paymentToken.token; 
        this.paymentService.paymobPaymentTokenSubject.next(paymentToken);
      }, 
      (resp)=>{
        console.log("error resp is : " , resp);
        alert("failed to get payment request")
      }
    )
  } 
 
}
