import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
  styleUrls: ['./card-form.component.css']
})
export class CardFormComponent implements OnInit {

  constructor(private domSanitizer: DomSanitizer) { }
  iframeUrl: string = "https://accept.paymob.com/api/acceptance/iframes/274814?payment_token=";

  @Input() paymentToken: string;
  ngOnInit(): void {
  }


  getIframeUrl() :SafeUrl
  {
    const res = this.iframeUrl + this.paymentToken;
    console.log("payment token is : " + this.paymentToken);
    return this.domSanitizer.bypassSecurityTrustResourceUrl(res);
  }

}
