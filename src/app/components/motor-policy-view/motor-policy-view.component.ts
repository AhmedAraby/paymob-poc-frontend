import { Component, OnInit } from '@angular/core';
import { PaymobPaymentTokenModel } from '../../models/paymob/paymob-payment-token.model';
import { PaymobPaymentTokenService } from '../../services/paymob-paymentToken.service';

@Component({
  selector: 'app-motor-policy-view',
  templateUrl: './motor-policy-view.component.html',
  styleUrls: ['./motor-policy-view.component.css']
})
export class MotorPolicyViewComponent implements OnInit {


  paymentTokenSub = null;
  paymentToken: PaymobPaymentTokenModel = null;
  
  constructor(private paymentService: PaymobPaymentTokenService) { 
    this.paymentService.paymobPaymentTokenSubject.subscribe(
      (paymentToken:PaymobPaymentTokenModel)=>{
        this.paymentToken = paymentToken;
      });
  }

  ngOnInit(): void {
  }

}
