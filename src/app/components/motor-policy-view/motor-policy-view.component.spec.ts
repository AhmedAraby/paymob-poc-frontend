import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MotorPolicyViewComponent } from './motor-policy-view.component';

describe('MotorPolicyViewComponent', () => {
  let component: MotorPolicyViewComponent;
  let fixture: ComponentFixture<MotorPolicyViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MotorPolicyViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MotorPolicyViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
