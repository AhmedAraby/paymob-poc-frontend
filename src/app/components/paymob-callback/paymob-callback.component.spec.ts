import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymobCallbackComponent } from './paymob-callback.component';

describe('PaymobCallbackComponent', () => {
  let component: PaymobCallbackComponent;
  let fixture: ComponentFixture<PaymobCallbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymobCallbackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymobCallbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
