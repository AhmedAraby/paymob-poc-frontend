import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-paymob-callback',
  templateUrl: './paymob-callback.component.html',
  styleUrls: ['./paymob-callback.component.css']
})
export class PaymobCallbackComponent implements OnInit {

  params;
  constructor(private activatedRoute: ActivatedRoute) {
    this.params = this.activatedRoute.snapshot.queryParams;
    console.log("params ", this.params); 
  }

  ngOnInit(): void {
  }

}
