import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PolicyOrderModel } from '../models/policy-order.model';

@Injectable({
  providedIn: 'root'
})


export class PaymobOrderService {
  createOrderApi: string = "http://localhost:8050/paymob/order";

  constructor(private httpClient: HttpClient) { }
  
  getPaymobOrder(
    paymobOrderRequst: PolicyOrderModel,
     customerId: number)
  {
    return this.httpClient.post(
      this.createOrderApi + "?customerId="+customerId , 
      paymobOrderRequst, 
      {observe:"body", responseType:"json"}
    );
  }
  
}
