import { TestBed } from '@angular/core/testing';

import { PaymobOrderService } from './paymob-order.service';

describe('PaymobOrderService', () => {
  let service: PaymobOrderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaymobOrderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
