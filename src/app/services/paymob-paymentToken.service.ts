import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import { Router } from '@angular/router';
import { URLSearchParams } from 'url';
import { PaymobCustomerBillingDataModel } from '../models/paymob/paymob-customer-billing-data.model';
import { BehaviorSubject } from 'rxjs';
import { PaymobPaymentTokenModel } from '../models/paymob/paymob-payment-token.model';
import { PolicyOrderModel } from '../models/policy-order.model';
import { PaymentTokenRequestModel } from '../models/payment-token-request.model';

@Injectable({
  providedIn: 'root'
})
export class PaymobPaymentTokenService {

  constructor(private httpClient: HttpClient) { }

  paymobPaymentTokenApi: string = "http://localhost:8050/paymob/payment_token";

  paymobPaymentTokenSubject: BehaviorSubject<PaymobPaymentTokenModel> = new BehaviorSubject(null);

  getpaymentToken(paymentTokenRequestModel: PaymentTokenRequestModel)
  {
    
    return this.httpClient.post(
      this.paymobPaymentTokenApi,
      paymentTokenRequestModel,
      {observe:"body", responseType:"json"},   
    );
  }

}
