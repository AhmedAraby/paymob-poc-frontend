import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CardTokenModel } from '../models/card-token.model';
import { map } from 'rxjs/operators';
import { RecurrentPaymentRequestModel } from '../models/recurrent-payment-request.model';

@Injectable({
  providedIn: 'root'
})
export class CardTokensService {

  cardTokensApi: string = "http://localhost:8050/tokens";

  constructor(private httpClient: HttpClient) { }

  getTokensByCustomerId(customerId: number)
  {
    return this.httpClient.get<CardTokenModel[]>(
      this.cardTokensApi + "?customerId="+customerId, 
      {observe:"body", responseType:"json"}
    ).pipe(
      map(
        (cardTokens: CardTokenModel[])=>{
          let newCardTokens: CardTokenModel[] = [];
          for(let token of cardTokens){
            token = CardTokenModel.createCardTokenModel(token);
            newCardTokens.push(token);
          }
          return newCardTokens;
        }
      ),
    );
  }

  payWithSavedCard(recurrentPaymentRequestModel: RecurrentPaymentRequestModel)
  {
    return this.httpClient.post(
      this.cardTokensApi + "/pay", 
      recurrentPaymentRequestModel, 
      {observe:"body", responseType:"json"},
    );
  }

}
