import { TestBed } from '@angular/core/testing';

import { CardTokensService } from './card-tokens.service';

describe('CardTokensService', () => {
  let service: CardTokensService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CardTokensService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
