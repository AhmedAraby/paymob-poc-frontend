import { TestBed } from '@angular/core/testing';

import { PaymobPaymentTokenService } from './paymob-paymentToken.service';

describe('PaymobPaymentTokenService', () => {
  let service: PaymobPaymentTokenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaymobPaymentTokenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
