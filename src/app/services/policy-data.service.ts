import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PolicyDataService {

  priceSubject: BehaviorSubject<number> = new BehaviorSubject(null);
  constructor() { }
}
