import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardFormComponent } from './components/card-form/card-form.component';
import { MotorPolicyComponent } from './components/motor-policy/motor-policy.component';
import { HttpClientModule } from '@angular/common/http';
import { PaymobCallbackComponent } from './components/paymob-callback/paymob-callback.component';
import { MotorPolicyViewComponent } from './components/motor-policy-view/motor-policy-view.component';
import { CardTokensComponent } from './components/card-tokens/card-tokens.component';
import { SaveCardComponent } from './components/save-card/save-card.component';
import { HomeComponent } from './components/home/home.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CardPaymentFormComponent } from './components/card-payment-form/card-payment-form.component';  // to be able to use it in templates

@NgModule({
  declarations: [
    AppComponent,
    CardFormComponent,
    MotorPolicyComponent,
    PaymobCallbackComponent,
    MotorPolicyViewComponent,
    CardTokensComponent,
    SaveCardComponent,
    HomeComponent,
    CardPaymentFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
