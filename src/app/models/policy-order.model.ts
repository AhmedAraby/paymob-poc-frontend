/**
 * policy order will be attached to paymob Order
 * after creating paymob order in the backEnd
 */

export class PolicyOrderModel
{
    constructor(
        public customerId: string, 
        public amountCents: string, public expiration: number,
        // integrationId is not needed in the policyOrder model at all
        //  but it is here untill we fix the backEnd and DataBase
        public currency: string, public lockOrderWhenPaid: boolean,
        public integrationId?: string, public paymobOrderId?: string){}
}