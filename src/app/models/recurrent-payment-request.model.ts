import { PaymobCustomerBillingDataModel } from "./paymob/paymob-customer-billing-data.model";
import { PolicyOrderModel } from "./policy-order.model";

export class RecurrentPaymentRequestModel
{
    constructor(
        public cardToken: string, 
        public policyOrder: PolicyOrderModel, 
        public paymobCustomerBillingData : PaymobCustomerBillingDataModel){}    
}