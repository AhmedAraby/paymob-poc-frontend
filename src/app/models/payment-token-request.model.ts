import { PaymobCustomerBillingDataModel } from "./paymob/paymob-customer-billing-data.model";
import { PolicyOrderModel } from "./policy-order.model";

export class PaymentTokenRequestModel
{
    constructor(
        public policyOrder: PolicyOrderModel, 
        public paymobCustomerBillingData: PaymobCustomerBillingDataModel
    ){}
}