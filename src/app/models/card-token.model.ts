/**
 * this class represent database entity
 */
export class CardTokenModel
{

    constructor(
        public id: string, public paymobTokenId: string, 
        public maskedPan: string, public token: string, 
        public merchantId: string, public cardSubStype: String, 
        public createdAt: Date, public email:string, 
        public paymobOrderId: string, public customerId: string
    ){}

    /**
     * 
     * @param cardToken is a JS raw object with similer properties as CardTokenModel.
     * @returns 
     */
    static createCardTokenModel(cardToken: CardTokenModel)
    {
        return new CardTokenModel(
            cardToken['id'], cardToken['paymobTokenId'], 
            cardToken['maskedPan'], cardToken['token'], 
            cardToken['merchantId'], cardToken['cardSubStype'],
            cardToken['createdAt'], cardToken['email'], 
            cardToken['paymobOrderId'], cardToken['customerId']
        );
    }

}