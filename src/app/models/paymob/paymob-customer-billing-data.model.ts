export class PaymobCustomerBillingDataModel
{
    constructor(
        public first_name: String, public last_name: string,
        public email:string, public phone_number: string)
    {}
}